using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ServerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = "a.txt";
            string fileContents = File.ReadAllText(filePath);

            IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
            Console.WriteLine("ipAddress"+ipAddress);
            int port = 12345;

            TcpListener listener = new TcpListener(ipAddress, port);
            listener.Start();

            Console.WriteLine("Waiting for a connection...");
            TcpClient client = listener.AcceptTcpClient();
            Console.WriteLine("Connected to client.");

            NetworkStream stream = client.GetStream();
            byte[] data = Encoding.UTF8.GetBytes(fileContents);
            stream.Write(data, 0, data.Length);

            stream.Close();
            client.Close();
            listener.Stop();
        }
    }
}