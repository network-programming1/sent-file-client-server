using System;
using System.Net.Sockets;
using System.Text;

namespace ClientApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string serverIP = "127.0.0.1";
            int port = 12345;

            TcpClient client = new TcpClient(serverIP, port);
            NetworkStream stream = client.GetStream();

            byte[] data = new byte[1024];
            int bytesRead;
            StringBuilder receivedData = new StringBuilder();

            do
            {
                bytesRead = stream.Read(data, 0, data.Length);
                receivedData.Append(Encoding.UTF8.GetString(data, 0, bytesRead));
            } while (bytesRead > 0);

            Console.WriteLine(receivedData.ToString());

            stream.Close();
            client.Close();
        }
    }
}
